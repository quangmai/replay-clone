import React from 'react';
import { StyleSheet, View, Text, Image, TouchableHighlight } from 'react-native';
import moment from '../utils/moment';

export default class MiniArticle extends React.PureComponent {
  isImageUri = thumbnail => !!thumbnail && thumbnail.startsWith('http');
  pressHandler = () => {
    this.props.navigation.navigate('Article', { permalink: this.props.permalink });
  };

  render() {
    return (
      <TouchableHighlight
        style={styles.container}
        activeOpacity={0.4}
        underlayColor="#BBDEFB"
        onPress={this.pressHandler}
      >
        <View style={styles.content}>
          <View style={styles.text_content}>
            <View style={styles.first_line}>
              <Text style={{ fontSize: 20, color: 'grey', paddingRight: 5 }}>
                {this.props.score}
              </Text>
              <Text style={{ color: 'blue', paddingRight: 5 }}>{this.props.author}</Text>
              <Text style={{ color: 'grey', paddingRight: 5 }}>in</Text>
              <Text style={{ color: 'blue' }}>{this.props.subreddit}</Text>
            </View>
            <View style={styles.second_line}>
              <Text style={{ fontWeight: 'bold' }}>{this.props.title}</Text>
            </View>
            <View style={styles.third_line}>
              {!!this.props.link_flair_text && (
                <Text style={{ paddingRight: 2 }}>{this.props.link_flair_text}</Text>
              )}
              {!!this.props.link_flair_text && <Text style={{ paddingRight: 2 }}>.</Text>}

              <Text style={{ color: 'grey', paddingRight: 2 }}>
                {this.props.num_comments} Comments
              </Text>
              <Text style={{ paddingRight: 2 }}>.</Text>
              <Text style={{ color: 'grey', paddingRight: 2 }}>{this.props.domain}</Text>
              <Text style={{ paddingRight: 2 }}>.</Text>
              <Text style={{ color: 'grey' }}>{moment.unix(this.props.created).fromNow(true)}</Text>
            </View>
          </View>
          {this.isImageUri(this.props.thumbnail) && (
            <View style={styles.thumbnail_container}>
              <Image style={styles.thumbnail} source={{ uri: this.props.thumbnail }} />
            </View>
          )}
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 10,
    marginBottom: 10,
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 3,
    elevation: 2,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  text_content: {
    flex: 1,
  },
  thumbnail_container: {
    marginLeft: 5,
  },
  thumbnail: {
    height: 70,
    width: 70,
  },
  first_line: {
    flexDirection: 'row',
    alignItems: 'baseline',
  },
  second_line: {
    marginVertical: 5,
  },
  third_line: {
    flexDirection: 'row',
    alignItems: 'baseline',
  },
});
