import React from 'react';
import { StyleSheet, Alert, View, Text, TouchableHighlight } from 'react-native';
import { Foundation, MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import ModalDropdown from 'react-native-modal-dropdown';
import { screenPaddingTop } from '../utils';
import TitleBarDropdown from './TitleBarDropdown';

export default class TitleBar extends React.Component {
  settingPressHandler = () => Alert.alert('Title', 'touched!');
  drawerPressHandler = () => this.props.navigation.navigate('DrawerOpen');

  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight
          activeOpacity={0.4}
          style={styles.btn}
          underlayColor="#4286f4"
          onPress={this.drawerPressHandler}
        >
          <Foundation name="list" size={35} color="white" />
        </TouchableHighlight>

        <TitleBarDropdown />

        <TouchableHighlight
          activeOpacity={0.4}
          style={styles.btn}
          underlayColor="#4286f4"
          onPress={this.settingPressHandler}
        >
          <MaterialIcons name="sort" size={35} color="white" />
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingTop: screenPaddingTop,
    height: screenPaddingTop + 56,
    backgroundColor: '#4286f4',
    justifyContent: 'space-between',
  },
  btn: {
    width: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
