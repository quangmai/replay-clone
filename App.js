import React from 'react';
import { screenPaddingTop } from './src/utils';
import { Platform, StyleSheet, Text, BaseScreen, StatusBar, ScrollView } from 'react-native';
import { DrawerNavigator, DrawerItems } from 'react-navigation';
import ArticleStackNavigator from './src/screens/ArticleStackNavigator';
import ProfileScreen from './src/screens/ProfileScreen';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducers from './src/reducers';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const store = createStoreWithMiddleware(reducers);

const AppNavigator = DrawerNavigator(
  {
    Home: {
      path: '/',
      screen: ArticleStackNavigator,
    },
    Profile: {
      path: '/profile',
      screen: ProfileScreen,
    },
  },
  {
    drawerWidth: 200,
    contentComponent: props => (
      <ScrollView style={{ paddingTop: screenPaddingTop }}>
        <DrawerItems {...props} />
      </ScrollView>
    ),
    initialRouteName: 'Home',
    contentOptions: {
      activeTintColor: '#e91e63',
    },
  },
);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    );
  }
}
