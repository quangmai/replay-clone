export const SELECT_SUBREDDIT = 'SELECT_SUBREDDIT';
export const RECEIVE_RESULT = 'RECEIVE_RESULT';
export const RECEIVE_ERROR = 'RECEIVE_ERROR';
export const LOAD_ARTICLE = 'LOAD_ARTICLE';
export const RECEIVE_ARTICLE = 'RECEIVE_ARTICLE';
export const RECEIVE_ARTICLE_ERROR = 'RECEIVE_ARTICLE_ERROR';

function fetchSubredditContent(subreddit, before, after) {
  let params = '?raw_json=1';

  if (before) {
    params += `&before=${before}`;
  } else if (after) {
    params += `&after=${after}`;
  }

  return fetch(`http://reddit.com/r/${subreddit}.json${params}`);
}

function fetchArticleData(permalink) {
  const segment = permalink.replace(/\/$/, '.json');
  console.log('segment', segment);
  return fetch(`http://reddit.com${segment}`);
}

export function selectSubreddit(subreddit) {
  return {
    type: SELECT_SUBREDDIT,
    subreddit,
  };
}

export function receiveResult(result) {
  return {
    type: RECEIVE_RESULT,
    result,
  };
}

export function receiveError(error) {
  return {
    type: RECEIVE_ERROR,
    error,
  };
}

// export function loadArticle(permalink) {
//   return {
//     type: LOAD_ARTICLE,
//     permalink,
//   };
// }

export function receiveArticle(result) {
  return {
    type: RECEIVE_ARTICLE,
    article: result[0],
    comments: result[1],
  };
}

export function receiveArticleError(error) {
  return {
    type: RECEIVE_ARTICLE_ERROR,
    error,
  };
}

// Thunk action
export function loadSubredditContent(subreddit, before, after) {
  return async (dispatch) => {
    try {
      const response = await fetchSubredditContent(subreddit, before, after);
      const responseJson = await response.json();
      dispatch(receiveResult(responseJson));
    } catch (error) {
      dispatch(receiveError(error));
    }
  };
}

// Thunk action
export function loadArticleData(permalink) {
  return async (dispatch) => {
    try {
      const response = await fetchArticleData(permalink);
      const responseJson = await response.json();
      dispatch(receiveArticle(responseJson));
    } catch (error) {
      dispatch(receiveArticleError(error));
    }
  };
}
