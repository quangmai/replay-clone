import React from 'react';
import { Animated, Easing, Dimensions } from 'react-native';
import { StackNavigator } from 'react-navigation';
import HomeScreen from './HomeScreen';
import ArticleScreen from './ArticleScreen';

const ModalStack = StackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        header: null,
      },
    },
    Article: {
      path: 'article/:id',
      screen: ArticleScreen,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    headerMode: 'none',
    mode: 'card',
    navigationOptions: {
      gesturesEnabled: true,
    },
  },
);

export default ModalStack;
