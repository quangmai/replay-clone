import React from 'react';
import { Text } from 'react-native';
import BaseScreen from './BaseScreen';
import { connect } from 'react-redux';

class ProfileScreen extends React.Component {
  render() {
    return (
      <BaseScreen {...this.props}>
        <Text>I'm</Text>
      </BaseScreen>
    );
  }
}

const mapStateToProps = (state) => {
  // TODO print all lifecycle events to see what did happen
  console.log('map something for ProfileScreen');
  return {
    subreddit: state.subreddit,
  };
};

export default connect(mapStateToProps)(ProfileScreen);
