import React from 'react';
import { StyleSheet, StatusBar, View, Text, FlatList } from 'react-native';
import TitleBar from '../components/TitleBar';

export default class BaseScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#4286f4" />
        <TitleBar navigation={this.props.navigation} />
        <View style={styles.content}>{this.props.children}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  content: {
    flex: 1,
  },
});
