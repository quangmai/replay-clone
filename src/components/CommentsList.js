import React, { Component } from 'react';
import { FlatList, Text } from 'react-native';
import Comment from './Comment';

class CommentsList extends Component {
  keyExtractor = (item, index) => item.data.id;

  renderComment = ({ item }) => {
    if (item.kind === 't1') {
      return <Comment data={item.data} level={this.props.level} />;
    }
    return <Text>TODO for load more feature</Text>;
  };

  render() {
    return (
      <FlatList
        data={this.props.comments}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderComment}
      />
    );
  }
}

export default CommentsList;
