import React from 'react';
import { StyleSheet, View, Text, Animated, TouchableWithoutFeedback } from 'react-native';
import CommentsList from './CommentsList';
import moment from '../utils/moment';

export default class Comment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
      animation: new Animated.Value(),
    };
  }

  toggle = () => {
    const initialValue = this.state.expanded
      ? this.state.maxHeight + this.state.minHeight
      : this.state.minHeight;
    const finalValue = this.state.expanded
      ? this.state.minHeight
      : this.state.maxHeight + this.state.minHeight;

    this.setState({
      ...this.state,
      expanded: !this.state.expanded,
    });

    console.log('initialValue', initialValue);
    console.log('finalValue', finalValue);

    this.state.animation.setValue(initialValue);

    Animated.spring(this.state.animation, {
      toValue: finalValue,
    }).start();
  };

  setMinHeight = (event) => {
    // console.log('setMinHeight', event.nativeEvent.layout);
    this.setState({
      ...this.state,
      minHeight: event.nativeEvent.layout.height,
    });
  };

  setMaxHeight = (event) => {
    // console.log('setMaxHeight', event.nativeEvent.layout);
    this.setState({
      ...this.state,
      maxHeight: event.nativeEvent.layout.height,
    });
  };

  renderRepliesSection() {
    // console.log('renderRepliesSection');
    if (this.props.data.replies && this.props.data.replies.data.children) {
      return (
        <View style={styles.replies_container}>
          <CommentsList
            comments={this.props.data.replies.data.children}
            level={++this.props.level}
          />
        </View>
      );
    }
  }

  render() {
    // console.log('Comment render');
    let containerColorCode = this.props.level % 5;
    if (this.props.level === 0) {
      containerColorCode = 0;
    } else if (containerColorCode === 0) {
      containerColorCode = 5;
    }

    return (
      <Animated.View style={{ height: this.state.animation }}>
        <View style={styles.container} onLayout={this.setMaxHeight}>
          <TouchableWithoutFeedback
            style={{ flex: 1 }}
            onPress={this.toggle}
            onLayout={this.setMinHeight}
          >
            <View style={[styles.comment, styles[`container_code_${containerColorCode}`]]}>
              <Text>
                {this.props.data.author} {' . '}
                {this.props.data.score} {' points '}
                {'. '}
                {moment.unix(this.props.data.created).fromNow(true)}
              </Text>
              <Text>{this.props.data.body}</Text>
            </View>
          </TouchableWithoutFeedback>
          {this.renderRepliesSection()}
        </View>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: '#bbb',
    borderTopWidth: 0.3,
    borderBottomWidth: 0.3,
  },
  comment: {
    flex: 1,
    padding: 5,
    backgroundColor: 'white',
    borderLeftWidth: 3,
  },
  replies_container: {
    flex: 1,
    marginLeft: 5,
  },
  container_code_0: {
    borderLeftWidth: 0,
  },
  container_code_1: {
    borderLeftColor: '#37befc',
  },
  container_code_2: {
    borderLeftColor: '#b217a8',
  },
  container_code_3: {
    borderLeftColor: '#7bb217',
  },
  container_code_4: {
    borderLeftColor: '#cad110',
  },
  container_code_5: {
    borderLeftColor: '#d10f0f',
  },
});
