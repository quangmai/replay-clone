import React from 'react';
import { StyleSheet, Alert, View, TouchableHighlight } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import ModalDropdown from 'react-native-modal-dropdown';
import { selectSubreddit } from '../actions';

class TitleBarDropdown extends React.Component {
  menuDownPressHandler = () => Alert.alert('Title', 'touched!');

  componentDidMount() {
    this.dropdown.select(0);
    this.props.selectOptionHandler(0, 'all');
  }

  render() {
    return (
      <View style={styles.modal_wrapper}>
        <ModalDropdown
          ref={dropdown => (this.dropdown = dropdown)}
          options={['all', 'pics', 'funny', 'news', 'worldnews', 'todayilearned']}
          defaultIndex={0}
          style={styles.modal}
          textStyle={styles.modal_text_style}
          dropdownStyle={styles.modal_drop_down_style}
          dropdownTextStyle={styles.modal_dropdown_text_style}
          onSelect={this.props.selectOptionHandler}
        />

        <TouchableHighlight
          activeOpacity={0.4}
          underlayColor="#4286f4"
          onPress={this.menuDownPressHandler}
        >
          <MaterialCommunityIcons
            style={styles.menu_down}
            name="menu-down"
            size={35}
            color="white"
          />
        </TouchableHighlight>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  selectOptionHandler: (index, subreddit) => {
    dispatch(selectSubreddit(subreddit));
  },
});

export default connect(undefined, mapDispatchToProps)(TitleBarDropdown);

const styles = StyleSheet.create({
  modal_wrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#4286f4',
  },
  modal: {
    width: 150,
    flexDirection: 'row',
    backgroundColor: '#4286f4',
  },
  menu_down: {},
  modal_text_style: {
    paddingLeft: 10,
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  modal_drop_down_style: {
    width: 200,
  },
  modal_dropdown_text_style: {
    fontSize: 18,
  },
  dropdown: {},
});
