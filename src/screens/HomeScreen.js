import React from 'react';
import { StyleSheet, FlatList, Button, View, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';

import BaseScreen from './BaseScreen';
import { loadSubredditContent } from '../actions';
import MiniArticle from '../components/MiniArticle';

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = this.getInitState();
  }

  getInitState = () => ({
    response: {},
    children: [],
    loading: false,
  });

  resetState = () => {
    this.setState(this.getInitState());
  };

  componentWillReceiveProps(newProps) {
    console.log('newProps', newProps.subreddit, this.props.subreddit);
    if (newProps.subreddit !== this.props.subreddit) {
      this.resetState();
      this.props.loadSubredditContent(newProps.subreddit);
    }

    if (newProps.response !== this.props.response) {
      console.log('new result');
      this.setState({
        ...this.state,
        response: newProps.response,
        children: this.state.children.concat(newProps.response.children).filter(Boolean),
        isEndReached: false,
      });
    }
  }

  idExtractor = (item, index) => item.data.id;
  renderItem = ({ item }) => <MiniArticle {...item.data} navigation={this.props.navigation} />;
  renderFooter = () => {
    if (!this.state.loading || !this.state.children.length) {
      return null;
    }

    return (
      <View style={styles.list_footer_component}>
        <ActivityIndicator animating size="large" style={styles.activity_indicator} />
      </View>
    );
  };

  endReachedHandler = () => {
    this.setState({ ...this.state, loading: true });
    // this.isEndReached = true;
    if (!this.onEndReachedCalledDuringMomentum) {
      this.props.loadSubredditContent(
        this.props.subreddit,
        this.props.response.before,
        this.props.response.after,
      );
      this.onEndReachedCalledDuringMomentum = true;
    }
  };

  refreshHandler = () => {
    this.props.loadSubredditContent(this.props.subreddit);
    this.resetState();
  };

  momentumScrollBeginHandler = () => {
    this.onEndReachedCalledDuringMomentum = false;
  };

  render() {
    console.log('HomeScreen - render');
    const { navigate } = this.props.navigation;
    return (
      <BaseScreen {...this.props}>
        <FlatList
          style={styles.flat_list}
          contentContainerStyle={styles.flat_list_content}
          data={this.state.children}
          keyExtractor={this.idExtractor}
          extraData={this.state}
          refreshing={this.props.loading}
          onRefresh={this.refreshHandler}
          onEndReachedThreshold={0.5}
          onEndReached={this.endReachedHandler}
          onMomentumScrollBegin={this.momentumScrollBeginHandler}
          renderItem={this.renderItem}
          ListFooterComponent={this.renderFooter}
        />
      </BaseScreen>
    );
  }
}

const mapStateToProps = (state) => {
  let response = {};

  if (state.loadingState.result && state.loadingState.result.data) {
    response = state.loadingState.result.data;
  }

  return {
    subreddit: state.navigationState.subreddit,
    response,
    loading: state.fetchState.loading,
  };
};

const mapDispatchToProps = dispatch => ({
  loadSubredditContent: (subreddit, before, after) =>
    dispatch(loadSubredditContent(subreddit, before, after)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

const styles = StyleSheet.create({
  flat_list: {},
  flat_list_content: {
    flexGrow: 1,
    paddingTop: 10,
    backgroundColor: '#E0E0E0',
  },
  list_footer_component: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  activity_indicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
  },
});
