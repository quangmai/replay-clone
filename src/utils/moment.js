import moment from 'moment';

moment.locale('en-mobile', {
  parentLocale: 'en',
  relativeTime: {
    future: 'in %s',
    past: '%s ',
    s: 's',
    m: 'min',
    mm: '%d min',
    h: 'hr',
    hh: '%d hr',
    d: 'd',
    dd: '%d d',
    M: 'a mth',
    MM: '%d mths',
    y: 'y',
    yy: '%d y',
  },
});

export default moment;
