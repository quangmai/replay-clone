import { combineReducers } from 'redux';
import {
  SELECT_SUBREDDIT,
  RECEIVE_ERROR,
  RECEIVE_RESULT,
  LOAD_ARTICLE,
  RECEIVE_ARTICLE,
  RECEIVE_ARTICLE_ERROR,
} from '../actions';

const initialNavigationState = {};
function navigationState(state = initialNavigationState, action) {
  switch (action.type) {
    case SELECT_SUBREDDIT:
      return Object.assign({}, state, {
        subreddit: action.subreddit,
        loading: true,
      });
    default:
      return state;
  }
}

const initialLoadingState = {
  result: {},
  article: {},
  comments: [],
  articleError: {},
};
function loadingState(state = initialLoadingState, action) {
  // console.log('action in reducer:', action);
  switch (action.type) {
    case RECEIVE_RESULT:
      return Object.assign({}, state, {
        result: action.result,
      });
    case RECEIVE_ERROR:
      return Object.assign({}, state, {
        result: action.error,
      });
    case RECEIVE_ARTICLE:
      return Object.assign({}, state, {
        article: action.article.data.children[0].data,
        comments: action.comments.data.children,
      });
    case RECEIVE_ARTICLE_ERROR:
      return Object.assign({}, state, {
        articleError: action.error,
      });
    default:
      return state;
  }
}

function fetchState(state = { loading: false }, action) {
  switch (action.type) {
    case SELECT_SUBREDDIT:
      return Object.assign({}, state, {
        loading: true,
      });
    case RECEIVE_ERROR:
    case RECEIVE_RESULT:
      return Object.assign({}, state, {
        loading: false,
      });
    case LOAD_ARTICLE:
      return Object.assign({}, state, {
        loadingArticle: false,
      });
    case RECEIVE_ARTICLE:
    case RECEIVE_ARTICLE_ERROR:
      return Object.assign({}, state, {
        loadingArticle: false,
      });
    default:
      return state;
  }
}

export default combineReducers({
  navigationState,
  loadingState,
  fetchState,
});
