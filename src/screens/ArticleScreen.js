import React from 'react';
import { Constants } from 'expo';
import {
  Platform,
  StyleSheet,
  FlatList,
  StatusBar,
  WebView,
  View,
  Text,
  ScrollView,
  Dimensions,
} from 'react-native';
import Swiper from 'react-native-swiper';
import CommentsList from '../components/CommentsList';
import { screenPaddingTop } from '../utils';
import { connect } from 'react-redux';
import { loadArticleData } from '../actions';

class ArticleScreen extends React.Component {
  componentDidMount() {
    this.props.loadData(this.props.navigation.state.params.permalink);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.article !== this.props.article || nextProps.comments !== this.props.comments) {
      console.log('forceUpdate');
      this.forceUpdate();
    }
  }

  render() {
    const { height, width } = Dimensions.get('window');
    const webViewHeight =
      height - Constants.statusBarHeight - StyleSheet.flatten(styles.status_bar).height;

    return (
      <ScrollView style={{ flex: 1 }} scrollEnabled>
        <StatusBar backgroundColor="#4286f4" />
        {!this.props.article.is_self && (
          <WebView
            source={{ uri: this.props.article.url }}
            pagingEnabled
            style={{
              flex: 1,
              marginTop: screenPaddingTop,
              height: webViewHeight,
              width,
            }}
          />
        )}
        <View style={styles.status_bar}>
          <Text>Some texts</Text>
        </View>
        <View style={{ minHeight: webViewHeight }}>
          <CommentsList comments={this.props.comments} level={0} />
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  article: state.loadingState.article,
  comments: state.loadingState.comments,
});

const mapDispatchToProps = dispatch => ({
  loadData: (permalink) => {
    dispatch(loadArticleData(permalink));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ArticleScreen);

const styles = StyleSheet.create({
  status_bar: {
    backgroundColor: 'blue',
    height: 50,
  },
  comment_section: {},
});
